'use strict'

let sortBuble = array => {
  for (let i = 0; i < array.length; i++) {
    for (let j = 0; j < array.length; j++) {
      if (array[i] < array[j]) {
        [ array[i], array[j] ] = [ array[j], array[i] ];
      }
    }
  }

  return array;
}

function createList(elementsCount, min, max) {
  let collection = {
    items: [],
    *[Symbol.iterator]() {
      for (let item of this.items) {
        yield item;
      }
    }
  };

  for (let i = 0; i < elementsCount; i++) {
    collection.items.push(Math.floor(Math.random() * (max - min) + min));
  }

  return collection;
}

let http = require('http');
let express = require('express');
let bodyParser = require('body-parser');

let app = express();
let server = http.createServer(app);

const PORT = process.env.PORT || 5000;
const IP = process.env.IP || "0.0.0.0";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

app.post('/', (req, res) => {
  let max = parseInt(req.body.max_field),
      min = parseInt(req.body.min_field),
      count = parseInt(req.body.elements_field);

  let list = createList(count, min, max);

  console.time('sort optimized');
  sortBuble(list.items);
  console.timeEnd('sort optimized');

  res.send({
    status: 'done!',
    data: {
      items: list.items.join(', ')
    }
  });
});

server.listen(PORT, IP, () => {
  console.log(`Server work on port ${PORT}...`);
});
